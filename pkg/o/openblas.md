* [Add function prototypes](https://github.com/xianyi/OpenBLAS/commit/f703846ad9400a8ea175cb8dd43e18c152aeab93)
* [Fix pointer/integer argument mismatch in calls to pow(](https://github.com/xianyi/OpenBLAS/commit/515cf269291bec0d43651fe7bf99a71fb074a0ad)
* [fix missing return type in function declaration](https://github.com/xianyi/OpenBLAS/commit/91110f92d218492d0efbdc1fdf34277ca45f4b36)
* [Fix missing external declaration](https://github.com/xianyi/OpenBLAS/commit/9402df5604e69f86f58953e3883f33f98c930baf)
* [Fix warnings](https://github.com/xianyi/OpenBLAS/commit/101a2c77c3f3610933f450cefca3e312edab2186)
* https://src.fedoraproject.org/rpms/openblas/c/5f27d51cebe1c1bb6598d38326ece8dc0ac71ec7?branch=rawhide
