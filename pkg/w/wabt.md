* [Fix architecture checks (#1969)](https://github.com/WebAssembly/wabt/commit/623fe3719aee656783f26a3ba854b5433313ccd9)
* https://src.fedoraproject.org/rpms/wabt/c/c48c1c9b20a340513087222485cd3e0089f14aa2?branch=rawhide
