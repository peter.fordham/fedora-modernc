* [configure.ac: Avoid implicit int, implicit function declarations](https://github.com/appneta/tcpreplay/pull/757)
* https://src.fedoraproject.org/rpms/tcpreplay/c/7a6770dd2e7cd907e5f194bbf65109fd74c85530?branch=rawhide
