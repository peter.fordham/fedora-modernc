* [Implicit int declarations are incompatible with C99](https://sourceforge.net/p/tix/bugs/112/)
* https://src.fedoraproject.org/rpms/tix/c/c35b75cfd57e7ea5c480e9321a89726fb187b8e1?branch=rawhide
