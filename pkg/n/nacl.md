* https://bugzilla.redhat.com/show_bug.cgi?id=2144813
* [Build in C89 mode](https://src.fedoraproject.org/rpms/nacl/c/d13416f2a9e6ff9a69046ee4fdb5f02b8956a45e?branch=rawhide)

No real fix because the package is broken on x86: It [corrupts the FPU
state](https://bugzilla.redhat.com/show_bug.cgi?id=2144809).
