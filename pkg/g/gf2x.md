* [Meta-ticket: Support Xcode 12](https://trac.sagemath.org/ticket/30494)
* [fix #include in configure test](https://gitlab.inria.fr/gf2x/gf2x/-/commit/a2f0fd388c12ca0b9f4525c6cfbc515418dcbaf8)
* https://src.fedoraproject.org/rpms/gf2x/c/aec8c4b7159f4395f5a213f1e8d1898c69a1f95e?branch=rawhide
