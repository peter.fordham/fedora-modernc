* [glfs-truncate: Add missing int return types](https://github.com/gluster/glusterfs-coreutils/pull/38)
* https://src.fedoraproject.org/rpms/glusterfs-coreutils/c/2ccd69a80b0084b64f14c02e8637056184bde219?branch=rawhide
