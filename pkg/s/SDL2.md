* [configure.ac: fix configure tests broken with Clang 15 (implicit function declarations)](https://github.com/libsdl-org/SDL/commit/d0a3570300812bc81888e7a7eadb9311621dc9a7)
* [regenerated configure script.](d936499670e4ec46c6f92633b129ab25b2208e75)
* [apply commit d0a3570300812bc81888e7a7eadb9311621dc9a7 to cmake side too.](https://github.com/libsdl-org/SDL/commit/5b2884cb0203cc63bf9753f8b55ea4c6c6f19cfb)
* https://src.fedoraproject.org/rpms/SDL2/c/202763ce81b707f471ab9a1f6849a511bef5b254?branch=rawhide
