* [Avoid relying on C89 features in a few places](https://gitlab.com/samba-team/samba/-/merge_requests/2807)
* https://src.fedoraproject.org/rpms/samba/c/22ec3a0a0d740ffe16f1d4013dbea9ff5aa24106?branch=rawhide
