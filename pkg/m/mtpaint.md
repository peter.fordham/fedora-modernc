* [configure: Improve C99 compatibility](https://github.com/wjaguar/mtPaint/pull/70)
* https://src.fedoraproject.org/rpms/mtpaint/c/a2ea6958f9f2345013be885f74a1761b2d987e40?branch=rawhide
