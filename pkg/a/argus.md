* [configure (AC_LBL_UNALIGNED_ACCESS): Avoid implicit function decls](https://github.com/openargus/argus/pull/5)
* [configure (AC_LBL_UNALIGNED_ACCESS): Avoid implicit function decls](https://github.com/openargus/clients/pull/7) (client code)
* https://src.fedoraproject.org/rpms/argus/c/7c333261e69c97a95995716f2de5afb518f5617d?branch=rawhide
