* [Use AC_SYS_LARGEFILE for large-file support](https://github.com/aide/aide/commit/ab12f8919f0f7beff0b8db974e98285ede6a285d)
* [Remove C99 compliant snprintf implementation](https://github.com/aide/aide/commit/909e656b8aca9a243f21b40dda3585f8d1ad809b)
* [Fix configure.ac compatibility with Clang 16](https://github.com/aide/aide/commit/601113f8a57c8f195af09bb2f14123449fa6bded)
* https://src.fedoraproject.org/rpms/aide/c/2681d69152546903b15b938d184a1f9796b284f7?branch=rawhide
